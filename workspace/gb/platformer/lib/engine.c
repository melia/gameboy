#ifndef _ENGINE_H
#define _ENGINE_H

#include <application.h>
#include <render.h>
#include "sound.c"
#include "timer.c"

typedef struct {
    module_application_t module_application;
    module_render_t module_render;
    module_sound_t module_sound;
    timer_t timer;
} engine_t;


void engine_init(engine_t *engine) {
    timer_init(&(engine->timer));
    engine->module_sound.sound_effects_enabled = TRUE;
}

#endif /* engine.c */
