#ifndef	_RENDER_C
#define	_RENDER_C

#include "engine.c"

#define RASTER_X_MIN 0

#define SECTION_SIZE 32 // section size 32x32 tiles

void re_reset(engine_t *engine) {
    //   @todo seperate to external configuration
    engine->module_render.camera.raster_x = 0;
    engine->module_render.camera.raster_y = 0;
    engine->module_render.camera.scroll_x = 0;
    engine->module_render.camera.scroll_y = 0;
    engine->module_render.camera.viewport.size_x = VIEWPORT_SIZE_X;
    engine->module_render.camera.viewport.size_y = VIEWPORT_SIZE_Y;
    engine->module_render.vblank_synchronisation = 1;
    engine->module_render.update_scene = TRUE;
}

UWORD re_retrieve_offset_scene(engine_t *engine) {
    return engine->module_render.camera.raster_x
            + (engine->module_render.camera.raster_y
                    * engine->module_render.camera.scene_width);
}

void re_update_camera(engine_t *engine) {
//    if (engine->module_render.camera.scroll_x >= TILE_WIDTH) {
//        //engine->module_render.camera.scroll_x = 0;
//        ++engine->module_render.camera.raster_x;
//        engine->update_scene = TRUE;
//    } else if (engine->module_render.camera.scroll_x <= -TILE_WIDTH) {
//        if (engine->module_render.camera.raster_x > 0) {
//            --engine->module_render.camera.raster_x;
//        }
//        //engine->module_render.camera.scroll_x = 0;
//        engine->update_scene = TRUE;
//    }

    engine->module_render.camera.raster_x =
            (int) (engine->module_render.camera.scroll_x / TILE_WIDTH);
    //            cameraraster_y = (int)(camera.positionY / TILE_HEIGHT);
}

BOOLEAN re_viewport_reached_minx(engine_t *engine) {
    return (0 == engine->module_render.camera.raster_x)
            && (0 == engine->module_render.camera.scroll_x);
}

BOOLEAN re_viewport_reached_maxx(engine_t *engine) {
    return (engine->module_render.camera.scene_width
            == engine->module_render.camera.raster_x);
}

BOOLEAN re_viewport_in_scope(engine_t *engine) {
    return (engine->module_render.camera.raster_x >= 0)
            && (engine->module_render.camera.raster_y >= 0);
}

void re_load_scene(engine_t *engine, UBYTE scene[]) {
    // define local variables
    UBYTE section;
    UWORD offset_scene = re_retrieve_offset_scene(engine);
    // @todo determine which section already have been loaded (strategy compare data offset)
    for (section = 0; section <= engine->module_render.camera.viewport.size_y;
            ++section) {
        set_bkg_tiles(0, section, engine->module_render.camera.viewport.size_x,
                1, &scene[offset_scene]);
        offset_scene += engine->module_render.camera.scene_width;
    }
}

void render_scene(module_render_t *module_render) {// @todo , UBYTE scene[] as argument
    if (1||module_render->update_scene) {
        wait_vbl_done(); // @todo move to render configuration
        printf("SCENE RENDERED\n");
    }
//    // update viewport
//    re_update_camera(engine);
//    // determine wether viewport remains in valid scope (will prevent data glitches)
//    if (re_viewport_in_scope(engine)) {
//        // review wether vblank synchronisation is enabled
//        if (engine->module_render.vblank_synchronisation) {
//            wait_vbl_done();
//        }
//        // render scene section wise (section with dimension defined before, default 32x32 tiles)
//        re_load_scene(engine, scene);
//        //        if (engine->update_scene) {
////            re_load_scene(engine, scene);
////            engine->update_scene = FALSE; // scene loaded
////        }
//        // correct position of background layer
//        move_bkg(engine->module_render.camera.scroll_x,
//                engine->module_render.camera.scroll_y);
//    }
}

void vertical_synchronisation(void) {
//    UINT8 event = waitjoy();
//    if (J_START == event) {
//        printf("start was pressed\n");
//    }
//    printf("event: %u", event);

}

void re_process_input(engine_t *engine, UBYTE event_button) {
    if (event_button & J_LEFT) {
        if (!re_viewport_reached_minx(engine)) {
            --engine->module_render.camera.scroll_x;
        }
    } else if (event_button & J_RIGHT) {
        if (!re_viewport_reached_maxx(engine)) {
            ++engine->module_render.camera.scroll_x;
        }
    }

    // correct position of background layer
    //scroll_bkg(engine->module_render.camera.scroll_x, engine->module_render.camera.scroll_y);
    //        if(event_button & J_UP){
    //            enine.camera.scroll_y--;
    //        }
    //        else if(event_button & J_DOWN){
    //            enine.camera.scroll_y++;
    //        }
}

UWORD re_offset_scene(scene_t *scene) {
    return scene->data.a.x + (scene->data.a.y * scene->level_width);
}

void re_draw_scene(scene_t *scene, UBYTE scene_data[]) {

    UBYTE index;
    UWORD width, height;
    UWORD offset_scene = re_offset_scene(scene);

    width = scene->data.b.x - scene->data.a.x; // @todo check if between 0 and 31
    height = scene->data.b.y - scene->data.a.y;

    for (index = 0; index <= height; ++index) {
        set_bkg_tiles(scene->viewport.x, scene->viewport.y, width, 1,
                &scene_data[offset_scene]);
    }
}

//#include "monitor.h"
//
///**
// * manipulate level, assign tile to provided
// * raster information
// */
//void set_tile(unsigned char *level[], int raster_x, int raster_y, UINT8 tile_index)
//{
//    *level[ raster_x + raster_y * MONITOR_WIDTH ] = tile_index; // manipulate level
//}

#endif
