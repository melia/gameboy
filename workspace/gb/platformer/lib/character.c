/*
 * character.c
 *
 *  Created on: 30.08.2014
 *      Author: lenovo
 */
#include "platformer.h"
#include "types.c"

int character_update_position(character_t *character) {
	int positionUpdated = POSITION_NOT_UPDATED;
    // refresh accelerationX (movement)
    if(character->positionXFixed == POSITION_NOT_FIXED){
    	character->boundary.point.positionX += character->accelerationX;
    	positionUpdated = POSITION_UPDATED;
    }
    // refresh accelerationY (gravitation)
    if(character->positionYFixed == POSITION_NOT_FIXED){
    	character->accelerationY += character->gravitation;
    	character->boundary.point.positionY += character->accelerationY;
    	positionUpdated = POSITION_UPDATED;
    }
    return positionUpdated;
}

// @todo change to boolean value
//int character_can_bounce(character_t *character) {
//	return (int)(character->positionYFixed == POSITION_FIXED);
//}

// @todo return value
// @todo seperate review wether character can bounce
void character_action_bounce(character_t *character) {
	if(character->positionYFixed == POSITION_FIXED){
        character->positionYFixed = POSITION_NOT_FIXED;
        character->accelerationY = -abs(BOUNCE_IMPULS);
	}
}


