#ifndef _TIMER_C
#define _TIMER_C

#include <time.h>
#include <timer.h>

UWORD get_current_time(void) {
    return clock() / CLOCKS_PER_SEC;
}

void timer_init(timer_t *timer) {
    timer->before = get_current_time();
}

void timer_update(timer_t *timer) {
    timer->current = get_current_time();
    timer->time_passed = timer->current - timer->before;
    timer->before = timer->current;
    timer->time_delta += timer->time_passed;
}

BOOLEAN timer_valid(timer_t *timer) {
    return ((0 <= timer->time_delta) && (TIME_PER_TICK <= timer->time_delta));
}

void timer_dispatch(timer_t *timer) {
    timer->time_delta -= TIME_PER_TICK;
}

//void waitms(int milliseconds) {
//   clock_t until;
//   until = clock()+(milliseconds*1000/CLOCKS_PER_SEC) ; /* 1,2,3...*/
//   while (clock() < until) {
//       time_delta(1);
//   }
//}

#endif /* timer.c */
