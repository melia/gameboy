#ifndef _INPUT_H
#define _INPUT_H 1

#include <gb/gb.h>

/**
 * Waits until any key is pressed. No mask is required.
 *
 * @TODO how to use less CPU resources without time_delta?
 */
UINT8 waitjoy() {
    UINT8 event;
    do {
        event = joypad();
    } while (0 == event);
    return event;
}

#endif /* input.h  */
