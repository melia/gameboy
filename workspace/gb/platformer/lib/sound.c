#ifndef _SOUND_H
#define _SOUND_H

extern const unsigned char * song_Data[];

// @todo implement sound dispatcher
// @todo implement engine module

typedef struct {
    BOOLEAN sound_effects_enabled;
} module_sound_t;


void module_sound_register(void) {
    disable_interrupts();

    add_VBL(gbt_update);

    set_interrupts(VBL_IFLAG);
    enable_interrupts();
}

void play_background_music(void) {
    ////////////////////////////////////////////////////////////////////
    gbt_play(song_Data, 2, 7);
    gbt_loop(1);
    /////////////////////////////////////////////////////////////////////
}

#endif /* sound.c */
