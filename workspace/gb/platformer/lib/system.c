#ifndef _SYSTEM_H
#define _SYSTEM_H 1

#define SYSTEM_INTERRUPTS_OFF 00000011
#define SYSTEM_INTERRUPTS_ON 00000001

#define SYSTEM_DISPLAY_OFF 00000100
#define SYSTEM_DISPLAY_ON 10010000

// @todo return current system state
// @todo external configuration of system state
// @todo implement persistant state to override local mask
// @todo pass mask as argument
//void switch_system_state(UBYTE state) {
//    UBYTE mask = 00000100; // @todo seperate mask
//    if((mask & state) == SYSTEM_DISPLAY_ON){
//        DISPLAY_ON;
//    }
//    if((mask & state) == SYSTEM_DISPLAY_OFF){
//        DISPLAY_OFF;
//    }
//    if((mask & state) == SYSTEM_INTERRUPTS_OFF){
//        disable_interrupts();
//    }
//    if((mask & state) == SYSTEM_INTERRUPTS_ON){
//        enable_interrupts();
//    }
//}

#endif /* system.c */
