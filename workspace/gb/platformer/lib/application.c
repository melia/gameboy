#ifndef _APPLICATION_C
#define _APPLICATION_C

#include <application.h>

BOOLEAN application_running(module_application_t *module_application) {
    return ((APPLICATION_STATE_RUNNING == module_application->state) || (APPLICATION_STATE_PAUSED != module_application->state));
}

#endif /* application.c */
