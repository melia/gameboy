#ifndef	_TYPES_H
#define	_TYPES_H 1

typedef struct {
    int positionX;
    int positionY;
} point_t;

typedef struct {
	point_t point;
	int height;
	int width;
} boundary_t;

typedef struct {
    int accelerationX;
    int accelerationY;
    int gravitation;
    int positionXFixed;
    int positionYFixed;
    boundary_t boundary;
} character_t;


#endif
