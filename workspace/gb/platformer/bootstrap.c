#include <stdlib.h>
#include <stdio.h>
#include <gb/gb.h>
#include <gb/drawing.h>
#include <string.h>
#include "vendor/gbt-player/gbt_player.h"

#include "lib/application.c"
#include "lib/platformer.h"
#include "lib/character.c"
#include "lib/monitor.h"
#include "lib/render.c"
#include "lib/system.c"
#include "lib/input.c"
#include "lib/timer.c"
//#include "lib/base64.c"
#include "lib/sound.c"
#include "lib/engine.c"
#include "data/level/level_001.c"
#include "data/tileset/tileset_001.h"
#include "data/tileset/tileset_001.c"

//void play_background_music();
//void module_sound_register();
//void modules_init(engine_t *engine);
