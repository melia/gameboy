#ifndef _RENDER_H
#define _RENDER_H

typedef struct {
    UBYTE size_x;
    UBYTE size_y;
} viewport_t;

typedef struct {
    UWORD x;
    UWORD y;
} coordinate_t;

typedef struct {
    coordinate_t a;
    coordinate_t b;
} dimension_t;

typedef struct {
    dimension_t data;
    coordinate_t viewport;
    UBYTE layer;
    UWORD level_width;
} scene_t;

typedef struct {
    UWORD scene_width;
    UWORD raster_x;
    UWORD raster_y;
    BYTE scroll_x;
    BYTE scroll_y;
    viewport_t viewport;
} camera_t;

typedef struct {
    BOOLEAN vblank_synchronisation;
    BOOLEAN update_scene;
    camera_t camera;
} module_render_t;

#endif /* render.h */
