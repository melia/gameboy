#ifndef _TIMER_H
#define _TIMER_H

#define TIME_PER_TICK 1 // measured in milliseconds

typedef struct struct_timer {
    UWORD current, before, time_passed; // @todo get rid of before variable
    WORD time_delta;
} timer_t;

#endif
