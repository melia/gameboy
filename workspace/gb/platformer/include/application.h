#ifndef _APPLICATION_H
#define _APPLICATION_H

#define APPLICATION_STATE_RUNNING 0
#define APPLICATION_STATE_PAUSED 1

typedef struct {
    UBYTE state;
} module_application_t;

BOOLEAN application_running(module_application_t *module_application);

#endif /* appication.h */
